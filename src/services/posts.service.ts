import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  baseUrl = 'https://dummyapi.io/data/api/tag/water/post?limit=10';
  APP_ID = '5f7b05bcf769b01d2660c2ad';


  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get(this.baseUrl, { headers: { 'app-id': this.APP_ID } });
  }
}
