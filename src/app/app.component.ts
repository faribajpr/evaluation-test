import { Component, OnInit } from '@angular/core';
import { PostsService } from 'src/services/posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'Evaluation-test';
  posts;
  t = [1, 2, 3, 4, 5, 6];

  constructor(private PostService: PostsService) {}


  ngOnInit() {
    this.PostService.getPosts().subscribe((post: any) => {
      if(post) {
        this.posts = post.data;
      } else {
        console.log('error');
      }
    });
  }


}
